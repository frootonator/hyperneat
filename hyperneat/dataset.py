import numpy as np


class Data:
    def __init__(self, features: np.ndarray, target: np.ndarray):
        self.features = features
        self.target = target


class DiabetesDataset:
    def __init__(self, file: str):
        with open(file, "r") as f:
            lines = f.readlines()

            bool_in = int(lines[0].split("=")[1])
            real_in = int(lines[1].split("=")[1])
            bool_out = int(lines[2].split("=")[1])
            real_out = int(lines[3].split("=")[1])
            train_len = int(lines[4].split("=")[1])
            valid_len = int(lines[5].split("=")[1])
            test_len = int(lines[6].split("=")[1])

            inputs = bool_in + real_in
            outputs = bool_out + real_out

            current_line = 7
            features_arr = []
            targets_arr = []
            for line in lines[current_line : current_line + train_len]:
                seq = [float(x) for x in line.split()]
                features = seq[:inputs]
                target = seq[-outputs:]
                features_arr.append(features)
                targets_arr.append(target)

            features_arr = np.array(features_arr)
            targets_arr = np.array(targets_arr)
            self.train_data = Data(features_arr, targets_arr)

            current_line += train_len

            features_arr = []
            targets_arr = []
            for line in lines[current_line : current_line + valid_len]:
                seq = [float(x) for x in line.split()]
                features = seq[:inputs]
                target = seq[-outputs:]
                features_arr.append(features)
                targets_arr.append(target)

            features_arr = np.array(features_arr)
            targets_arr = np.array(targets_arr)
            self.valid_data = Data(features_arr, targets_arr)

            current_line += valid_len

            features_arr = []
            targets_arr = []
            for line in lines[current_line : current_line + test_len]:
                seq = [float(x) for x in line.split()]
                features = seq[:inputs]
                target = seq[-outputs:]
                features_arr.append(features)
                targets_arr.append(target)

            features_arr = np.array(features_arr)
            targets_arr = np.array(targets_arr)
            self.test_data = Data(features_arr, targets_arr)
